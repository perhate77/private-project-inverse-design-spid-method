These are inverse design codes for self assembly of charged colloids with hard-core repulsive Yukawa potential, written in C.

There are three codes:

1. Tuning one parameter

2.  Tuning two parameters

3.  Tuning three parameters

In these codes, I used the following sub algorithm,
1. NPT Monte Carlo Simulation
2. Solid Angle based Nearest-Neighbor algorithm (SANN)
3. Nearest Neighbor bond order parameter, q_6 and w_6
4. Statistical Physics-inspired Inverse Design algorithm (SPID)

Ref: Rajneesh Kumar, Gabriele Coli, Marjolein Dijkstra and Srikanth Sastry, Inverse design
of charged colloidal particle interactions for self assembly into specified crystal structures, J. Chem. Phys. 151, 084109 (2019).


Compile using:

*           gcc -I/home/rajneesh/gsl/include -c OptimizedInvDesgnChrgdCollds1Paramtr.c -ffast-math -O3


*           gcc -L/home/rajneesh/gsl/lib OptimizedInvDesgnChrgdCollds1Paramtr.o -lgsl -lgslcblas -lm


*           export LD_LIBRARY_PATH=/home/rajneesh/gsl/lib


*           nohup ./a.out &


Change path of the gsl libraies to the path where you have installed gsl in your system.
if gsl is installed in default (root) directories, compile using:


*           gcc OptimizedInvDesgnChrgdCollds1Paramtr.c -lgsl -lgslcblas -lm -ffast-math -O3


*           nohup ./a.out &


Written By::<br/>
Rajneesh Kumar<br/>
Theoretical Sciences Unit, Jawaharlal Nehru Centre for Advanced Scientific Research,<br/>
Bengaluru 560064, India.<br/>
Email: rajneesh[at]jncasr.ac.in<br/>
27 Dec, 2020
